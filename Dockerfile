FROM maven:3.6.0-jdk-11-slim AS build
RUN mkdir -p /opt/app
COPY ./ /opt/app
RUN mvn -f /opt/app/pom.xml clean package -Dmaven.test.skip=true -B

FROM openjdk:11.0.2-jdk-slim
RUN mkdir -p /opt/app
COPY --from=build /opt/app/target/jumbo-demo.jar /opt/app
ARG JDK_SPECIFIC_CONFIG="-Djava.security.egd=file:/dev/./urandom "
ARG DEFAULT_JAVA_OPTS="-Xms256m -Xmx1024m $JDK_SPECIFIC_CONFIG"
ENV JAVA_OPTS=$DEFAULT_JAVA_OPTS

ENTRYPOINT java $JAVA_OPTS -jar /opt/app/jumbo-demo.jar
EXPOSE 8080

