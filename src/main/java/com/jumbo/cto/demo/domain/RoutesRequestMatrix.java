package com.jumbo.cto.demo.domain;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@ApiModel(description = "Input data for many routes matrix. destinationPostalCode --> House originPostalCodes--> Stores")
public class RoutesRequestMatrix {

    @NotEmpty
    @Size(min = 1, max = 25)
    @Valid
    private List<String> originPostalCodes;

    @NotNull
    @Valid
    private String destinationPostalCode;
}
