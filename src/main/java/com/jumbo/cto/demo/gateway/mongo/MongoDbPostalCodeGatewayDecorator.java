package com.jumbo.cto.demo.gateway.mongo;

import com.jumbo.cto.demo.domain.LatLong;
import com.jumbo.cto.demo.domain.Route;
import com.jumbo.cto.demo.gateway.RouteplannerGateway;
import com.jumbo.cto.demo.gateway.mongo.converter.RouteDocumentToRoute;
import com.jumbo.cto.demo.gateway.mongo.converter.RouteToRouteDocument;
import com.jumbo.cto.demo.gateway.mongo.document.*;
import com.jumbo.cto.demo.gateway.mongo.repository.PostalCodeRepository;
import com.jumbo.cto.demo.gateway.mongo.repository.RouteRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.DuplicateKeyException;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@RequiredArgsConstructor
@Slf4j
public class MongoDbPostalCodeGatewayDecorator implements RouteplannerGateway {

    private static final String MONGODB = "mongodb";
    private static final String OPERATION = "operation";
    private static final String COLLECTION = "collection";
    private final RouteplannerGateway decorated;
    private final PostalCodeRepository postalCodeRepository;
    private final RouteRepository routeRepository;
    private final RouteDocumentToRoute routeDocumentToRoute;
    private final RouteToRouteDocument routeToRouteDocument;


    @Override
    public Optional<LatLong> getCoordinates(final String postalCode, final String street) {

        final Optional<PostalCodeDocument> document = findPostCodeById(PostalCodeID.builder()
                .streetName(street)
                .postalCode(postalCode)
                .build());

        if (document.isPresent()) {
            if (document.get().getLatitude() == null && document.get().getLongitude() == null) {
                return Optional.empty();
            }

            return document.map(p -> LatLong.builder()
                    .lat(p.getLatitude())
                    .lng(p.getLongitude())
                    .build());
        }

        final Optional<LatLong> latLongOption = decorated.getCoordinates(postalCode, street);
        final LatLong latlong = latLongOption.orElse(new LatLong());

        try {
            savePostalCode(postalCode, street, latlong);
        } catch (DuplicateKeyException e) {
            log.error(e.getMessage(), e);
        }

        return latLongOption;
    }

    @Override
    public Collection<Route> getRoute(@NotEmpty @Size(min = 1, max = 25) @Valid List<String> origins, @NotNull @Valid String destination) {

        List<RouteDocumentID> ids = origins.stream()
                .map(origin -> createRouteDocumentID(origin, destination))
                .collect(Collectors.toList());

        final Iterable<RouteDocument> documents = findLatLongDistanceAndTimeByIds(ids);

        final List<Route> routes = StreamSupport.stream(documents.spliterator(), false)
                .map(routeDocumentToRoute::convert)
                .collect(Collectors.toList());

        if (routes.isEmpty()) {
            final Collection<Route> routesFromDecorated = decorated.getRoute(origins, destination);
            routes.addAll(routesFromDecorated);
        }

        saveRoutes(routes);

        return routes;
    }

    private void saveRoutes(Collection<Route> routes) {
        if (!routes.isEmpty()) {
            final List<RouteDocument> documents = routes.stream()
                    .map(routeToRouteDocument::convert)
                    .collect(Collectors.toList());

            documents.forEach(document -> document.setCreatedDate(LocalDateTime.now()));

            log.debug("saveAll {}", documents);

            routeRepository.saveAll(documents);
        }

    }

    private Iterable<RouteDocument> findLatLongDistanceAndTimeByIds(final List<RouteDocumentID> ids) {
        return routeRepository.findAllById(ids);
    }

    private Optional<PostalCodeDocument> findPostCodeById(PostalCodeID id) {
        log.debug("findById {}", id);
        return postalCodeRepository.findById(id);
    }

    private void savePostalCode(final String postalCode, final String street, final LatLong latlong) {
        final PostalCodeDocument document = PostalCodeDocument.builder()
                .latitude(latlong.getLat())
                .longitude(latlong.getLng())
                .createdDate(LocalDateTime.now())
                .postalCodeID(
                        PostalCodeID.builder()
                                .postalCode(postalCode)
                                .streetName(street)
                                .build())
                .build();
        log.debug("save {}", document);
        postalCodeRepository.save(document);
    }

    private RouteDocumentID createRouteDocumentID(String origin, String destination) {
        return RouteDocumentID.builder()
                .destinationPostalCode(destination)
                .originPostalCode(origin)
                .build();
    }


}