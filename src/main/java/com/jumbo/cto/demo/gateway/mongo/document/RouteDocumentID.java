package com.jumbo.cto.demo.gateway.mongo.document;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

@Data
@Builder
@AllArgsConstructor
public class RouteDocumentID implements Serializable {
    private String originPostalCode;
    private String destinationPostalCode;
}
