package com.jumbo.cto.demo.gateway.mongo.repository;

import com.jumbo.cto.demo.gateway.mongo.document.PostalCodeDocument;
import com.jumbo.cto.demo.gateway.mongo.document.PostalCodeID;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface PostalCodeRepository extends PagingAndSortingRepository<PostalCodeDocument, PostalCodeID> {
}
