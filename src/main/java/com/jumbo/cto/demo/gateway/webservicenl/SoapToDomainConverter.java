package com.jumbo.cto.demo.gateway.webservicenl;

import com.jumbo.cto.demo.domain.LatLong;
import com.jumbo.cto.demo.domain.Route;
import nl.webservices.soap.GeoLocationAddressCoordinatesLatLonResponseType;
import nl.webservices.soap.RoutePlannerInformationResponseType;

import java.util.Optional;

class SoapToDomainConverter {

    public static Route convert(RoutePlannerInformationResponseType responseType){
        Route route = new Route();

        route.setDistanceInMeters(Double.valueOf(responseType.getRoute().getDistance()));
        route.setTimeInSeconds(Long.valueOf(responseType.getRoute().getTime()));

        return route;
    }

    public static Optional<LatLong> convert(GeoLocationAddressCoordinatesLatLonResponseType responseType){
        LatLong latLong = new LatLong();

        latLong.setLat(Double.valueOf(responseType.getCoordinates().getLatitude()));
        latLong.setLng(Double.valueOf(responseType.getCoordinates().getLongitude()));

        return Optional.of(latLong);
    }
}
