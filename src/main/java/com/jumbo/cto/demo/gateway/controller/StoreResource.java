package com.jumbo.cto.demo.gateway.controller;

import com.jumbo.cto.demo.gateway.mongo.document.StoreDocument;
import com.jumbo.cto.demo.gateway.mongo.repository.StoreRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequiredArgsConstructor
@Slf4j
@RequestMapping("/stores")
public class StoreResource {

    private final StoreRepository storeRepository;

    @GetMapping("/")
    List<StoreDocument> getStores() {
        return (List<StoreDocument>) storeRepository.findAll();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Optional<StoreDocument> getStoreById(@PathVariable("id") String id) {
        return storeRepository.findById(id);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public void modifyStoreById(@PathVariable("id") String id, @Valid @RequestBody StoreDocument storeDocument) {
        storeDocument.setUuid(id);
        storeRepository.save(storeDocument);
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    public StoreDocument createStore(@Valid @RequestBody StoreDocument storeDocument) {
        storeRepository.save(storeDocument);
        return storeDocument;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void deleteStore(@PathVariable("id") String id) {
        storeRepository.deleteById(id);
    }
}
