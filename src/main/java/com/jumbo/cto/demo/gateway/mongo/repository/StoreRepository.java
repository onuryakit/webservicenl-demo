package com.jumbo.cto.demo.gateway.mongo.repository;

import com.jumbo.cto.demo.gateway.mongo.document.StoreDocument;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface StoreRepository extends PagingAndSortingRepository<StoreDocument,String> {
}
