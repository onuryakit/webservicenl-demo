package com.jumbo.cto.demo.gateway.mongo.document;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;

@AllArgsConstructor
@Builder
@Data
@Document(collection = "Route")
public class RouteDocument {

    @Id
    RouteDocumentID routeDocumentID;

    private int timeInSeconds;
    private int distanceInMeters;

    @Indexed(expireAfterSeconds = 2160000, background = true) // Expires after 25 days
    private LocalDateTime createdDate;
}
