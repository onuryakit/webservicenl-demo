package com.jumbo.cto.demo.gateway;

import com.jumbo.cto.demo.domain.LatLong;
import com.jumbo.cto.demo.domain.Route;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

public interface RouteplannerGateway {

    Optional<LatLong> getCoordinates(String postalCode, String street);

    Collection<Route> getRoute(final @NotEmpty @Size(min = 1, max = 25) @Valid List<String> origins, final @NotNull @Valid String destination);
}
