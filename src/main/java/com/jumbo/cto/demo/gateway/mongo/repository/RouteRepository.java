package com.jumbo.cto.demo.gateway.mongo.repository;

import com.jumbo.cto.demo.gateway.mongo.document.RouteDocument;
import com.jumbo.cto.demo.gateway.mongo.document.RouteDocumentID;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface RouteRepository  extends PagingAndSortingRepository<RouteDocument, RouteDocumentID> {
}
