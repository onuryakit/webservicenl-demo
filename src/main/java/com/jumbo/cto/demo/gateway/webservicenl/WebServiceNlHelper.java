package com.jumbo.cto.demo.gateway.webservicenl;

import com.webservicenl.WebServiceNLProxy;
import nl.webservices.soap.*;

class WebServiceNlHelper {
    private static final ObjectFactory factory = new ObjectFactory();

    private WebServiceNlHelper(){

    }

    public static GeoLocationAddressCoordinatesLatLonResponseType geoLocationAddressCoordinatesLatLon(String postalCode) {
        GeoLocationAddressCoordinatesLatLonRequestType request = factory.createGeoLocationAddressCoordinatesLatLonRequestType();
        request.setPostcode(postalCode);
        return WebServiceNLProxy.getInstance().geoLocationAddressCoordinatesLatLon(request);
    }

    public static RoutePlannerInformationResponseType routePlannerInformation(String origin, String destination) {
        RoutePlannerInformationRequestType request = factory.createRoutePlannerInformationRequestType();
        request.setPostcodefrom(origin);
        request.setPostcodeto(destination);
        request.setRoutetype("fastest");

        return WebServiceNLProxy.getInstance().routePlannerInformation(request);
    }
}
