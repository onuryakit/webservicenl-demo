package com.jumbo.cto.demo.gateway.mongo.converter;

import com.jumbo.cto.demo.domain.Route;
import com.jumbo.cto.demo.gateway.mongo.document.RouteDocument;
import com.jumbo.cto.demo.gateway.mongo.document.RouteDocumentID;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class RouteToRouteDocument implements Converter<Route, RouteDocument> {

    public RouteDocument convert(Route route) {
        return RouteDocument.builder()
                .distanceInMeters(route.getDistanceInMeters().intValue())
                .timeInSeconds(route.getTimeInSeconds().intValue())
                .routeDocumentID(RouteDocumentID.builder().destinationPostalCode(route.getDestinationPostalCode()).originPostalCode(route.getOriginPostalCode()).build())
                .build();
    }
}
