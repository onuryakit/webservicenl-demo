package com.jumbo.cto.demo.gateway.webservicenl;

import com.jumbo.cto.demo.domain.LatLong;
import com.jumbo.cto.demo.domain.Route;
import com.jumbo.cto.demo.gateway.RouteplannerGateway;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.webservices.soap.GeoLocationAddressCoordinatesLatLonResponseType;
import nl.webservices.soap.RoutePlannerInformationResponseType;
import org.springframework.stereotype.Component;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Component
@Slf4j
@RequiredArgsConstructor
public class WebServiceNLGateway implements RouteplannerGateway {

    @Override
    public Optional<LatLong> getCoordinates(String postalCode, String street) {
        GeoLocationAddressCoordinatesLatLonResponseType result = WebServiceNlHelper.geoLocationAddressCoordinatesLatLon(postalCode);
        return SoapToDomainConverter.convert(result);
    }

    @Override
    public Collection<Route> getRoute(@NotEmpty @Size(min = 1, max = 25) @Valid List<String> origins, @NotNull @Valid String destination) {
        Collection<Route> routes = new ArrayList<>();

        for(String origin : origins){
            final RoutePlannerInformationResponseType result = WebServiceNlHelper.routePlannerInformation(origin, destination);

            Route route = SoapToDomainConverter.convert(result);
            routes.add(route);
            route.setOriginPostalCode(origin);
            route.setDestinationPostalCode(destination);
        }

        return routes;
    }
}
