package com.jumbo.cto.demo.gateway.mongo.document;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document(collection = "stores")
@Builder
@CompoundIndexes(
        {
                @CompoundIndex(def = "{'_id.stores' : 1}", background = true),
                @CompoundIndex(def = "{'_id.stores' : 1, '_id.city' : 1}", background = true),
                @CompoundIndex(def = "{'_id.stores' : 1, '_id.city' : 1, '_id.streetName' : 1}", unique = true, sparse = true, background = true),
        }
)
public class StoreDocument {

    @Id
    String uuid;

    String city;
    String postalCode;
    String street;
    String street2;
    String addressName;
    Double longitude;
    Double latitude;
    String complexNumber;
    boolean showWarningMessage;
    String todayOpen;
    String locationType;
    String collectionPoint;
    String sapStoreID;
    String todayClose;

}
