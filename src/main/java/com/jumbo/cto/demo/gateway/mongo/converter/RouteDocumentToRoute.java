package com.jumbo.cto.demo.gateway.mongo.converter;

import com.jumbo.cto.demo.domain.Route;
import com.jumbo.cto.demo.gateway.mongo.document.RouteDocument;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class RouteDocumentToRoute implements Converter<RouteDocument, Route> {
    public Route convert(RouteDocument routeDocument) {
        return Route.builder()
                .destinationPostalCode(routeDocument.getRouteDocumentID().getDestinationPostalCode())
                .originPostalCode(routeDocument.getRouteDocumentID().getOriginPostalCode())
                .distanceInMeters(Double.valueOf(routeDocument.getDistanceInMeters()))
                .timeInSeconds(Long.valueOf(routeDocument.getTimeInSeconds()))
                .build();
    }
}
