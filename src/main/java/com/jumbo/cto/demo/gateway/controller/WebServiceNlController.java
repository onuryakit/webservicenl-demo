package com.jumbo.cto.demo.gateway.controller;


import com.jumbo.cto.demo.domain.LatLong;
import com.jumbo.cto.demo.domain.LatLongByAddress;
import com.jumbo.cto.demo.domain.Route;
import com.jumbo.cto.demo.domain.RoutesRequestMatrix;
import com.jumbo.cto.demo.usecase.UseCase;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Collection;
import java.util.Optional;

import static org.springframework.http.ResponseEntity.notFound;

@RestController
@RequiredArgsConstructor
@Slf4j
@RequestMapping("/webservicenl")
public class WebServiceNlController {

    private final UseCase<RoutesRequestMatrix, Collection<Route>> getRoute;
    private final UseCase<LatLongByAddress, Optional<LatLong>> getCoordinates;

    @GetMapping(value = "coordinates")
    @ApiOperation("Retrieve information about the given postal code")
    @ApiResponses({
            @ApiResponse(code = 200, message = "API called successfully.", response = LatLong.class),
            @ApiResponse(code = 404, message = "Not Found"),
    })
    public ResponseEntity<LatLong> getCoordinates(@Valid LatLongByAddress query) {
        return getCoordinates.execute(query)
                .map(ResponseEntity::ok)
                .orElse(notFound().build());
    }

    /**
     *   See the example payload.
     *
     *   {
     *   "destinationPostalCode": "5628PZ",
     *   "originPostalCodes": [
     *     "5615ST","5233AN","5615ST","5233AN","5233AN",
     *     "5615ST","5233AN","5615ST","5233AN","5233AN",
     *     "5615ST","5233AN","5615ST","5233AN","5233AN",
     *     "5615ST","5233AN","5615ST","5233AN","5233AN",
     *     "5615ST","5233AN","5615ST","5233AN","5233AN"
     *   ]
     * }
     * @param query
     * @return
     */
    @PostMapping(value = "routes")
    @ApiOperation("Retrieve route information")
    @ApiResponses({
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 200, message = "API called successfully.", response = Route.class),

    })
    public ResponseEntity<Collection<Route>> getRoute(@RequestBody @Valid RoutesRequestMatrix query) {
        final Collection<Route> latAndLongTimeDistances = getRoute.execute(query);
        if (latAndLongTimeDistances.isEmpty()) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(latAndLongTimeDistances);
    }


}
