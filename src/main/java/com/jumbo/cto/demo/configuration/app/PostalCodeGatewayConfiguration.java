package com.jumbo.cto.demo.configuration.app;

import com.jumbo.cto.demo.gateway.RouteplannerGateway;
import com.jumbo.cto.demo.gateway.mongo.MongoDbPostalCodeGatewayDecorator;
import com.jumbo.cto.demo.gateway.mongo.converter.RouteDocumentToRoute;
import com.jumbo.cto.demo.gateway.mongo.converter.RouteToRouteDocument;
import com.jumbo.cto.demo.gateway.mongo.repository.PostalCodeRepository;
import com.jumbo.cto.demo.gateway.mongo.repository.RouteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

@Configuration
public class PostalCodeGatewayConfiguration {
    private final RouteplannerGateway routeplannerGateway;
    private final PostalCodeRepository postalCodeRepository;
    private final RouteRepository routeRepository;
    private final RouteDocumentToRoute routeDocumentToRoute;
    private final RouteToRouteDocument routeToRouteDocument;

    @Autowired
    public PostalCodeGatewayConfiguration(@Qualifier("webServiceNLGateway") RouteplannerGateway routeplannerGateway,
                                          PostalCodeRepository postalCodeRepository,
                                          RouteRepository routeRepository,
                                          RouteDocumentToRoute routeDocumentToRoute,
                                          RouteToRouteDocument routeToRouteDocument) {
        this.routeplannerGateway = routeplannerGateway;
        this.postalCodeRepository = postalCodeRepository;
        this.routeRepository = routeRepository;
        this.routeDocumentToRoute = routeDocumentToRoute;
        this.routeToRouteDocument = routeToRouteDocument;
    }

    @Primary
    @Bean
    public RouteplannerGateway getRouteplannerGateway() {
        return new MongoDbPostalCodeGatewayDecorator(
                routeplannerGateway,
                postalCodeRepository,
                routeRepository,
                routeDocumentToRoute,
                routeToRouteDocument);
    }
}
