package com.jumbo.cto.demo.usecase;

import com.jumbo.cto.demo.domain.Route;
import com.jumbo.cto.demo.domain.RoutesRequestMatrix;
import com.jumbo.cto.demo.gateway.RouteplannerGateway;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Collection;

@RequiredArgsConstructor
@Service
public class GetRoute implements UseCase<RoutesRequestMatrix, Collection<Route>> {

    private final RouteplannerGateway routeplannerGateway;

    @Override
    public Collection<Route> execute(RoutesRequestMatrix query) {
        return routeplannerGateway.getRoute(
                query.getOriginPostalCodes(),
                query.getDestinationPostalCode());
    }

}
