package com.jumbo.cto.demo.usecase;

import com.jumbo.cto.demo.domain.LatLong;
import com.jumbo.cto.demo.domain.LatLongByAddress;
import com.jumbo.cto.demo.gateway.RouteplannerGateway;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@RequiredArgsConstructor
@Service
public class GetCoordinates implements  UseCase<LatLongByAddress, Optional<LatLong>> {

    private final RouteplannerGateway postalCodeGateway;

    @Override
    public Optional<LatLong> execute(LatLongByAddress query) {
        return postalCodeGateway.getCoordinates(query.getPostalCode(),query.getStreet());
    }
}
