package com.jumbo.cto.demo.gateway.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jumbo.cto.demo.domain.LatLong;
import com.jumbo.cto.demo.domain.LatLongByAddress;
import com.jumbo.cto.demo.domain.Route;
import com.jumbo.cto.demo.domain.RoutesRequestMatrix;
import com.jumbo.cto.demo.usecase.UseCase;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.BDDAssertions.then;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
public class WebServiceNlControllerTest {

    @Mock
    private UseCase<RoutesRequestMatrix, Collection<Route>> getRoute;
    @Mock
    private UseCase<LatLongByAddress, Optional<LatLong>> getCoordinates;
    private MockMvc mockMvc;
    private ObjectMapper objectMapper;

    @Before
    public void setUp() {
        WebServiceNlController webServiceNlController = new WebServiceNlController(getRoute, getCoordinates);
        mockMvc = MockMvcBuilders.standaloneSetup(webServiceNlController)
                .build();
        objectMapper = new ObjectMapper();
    }

    @Test
    public void shouldReturnLatLongForValidPostalCode() throws Exception {
        //GIVEN
        String postalCode = "5233AN";
        String country = "NL";

        //WHEN
        LatLong expected = LatLong.builder()
                .lat(51.71208572387695)
                .lng(5.313855171203613)
                .build();

        when(getCoordinates.execute(any())).thenReturn(Optional.of(expected));

        //THEN
        mockMvc.perform(
                get("/webservicenl/coordinates")
                        .accept(APPLICATION_JSON_UTF8)
                        .param("postalCode", postalCode)
                        .param("countryCode", country))
                // THEN the latitude should be retrieved successfully
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(expected)));
        verify(getCoordinates, only()).execute(any());
    }

    @Test
    public void shouldReturnRouteForValidRoutesRequestMatrix() throws Exception {
        //GIVEN
        String destinationPostalCode = "5233AN";
        List<String> postalCodes = Arrays.asList("5615ST", "5628PZ", "5616VD");

        RoutesRequestMatrix given = RoutesRequestMatrix.builder().originPostalCodes(postalCodes).destinationPostalCode(destinationPostalCode).build();

        //WHEN
        Route expectedRoute5615ST = Route.builder().timeInSeconds(1866L).distanceInMeters(Double.valueOf(42558)).originPostalCode("5615ST").destinationPostalCode("5233AN").build();
        Route expectedRoute5628PZ = Route.builder().timeInSeconds(1762L).distanceInMeters(Double.valueOf(39315)).originPostalCode("5628PZ").destinationPostalCode("5233AN").build();
        Route expectedRoute5616VD = Route.builder().timeInSeconds(1740L).distanceInMeters(Double.valueOf(40868)).originPostalCode("5616VD").destinationPostalCode("5233AN").build();

        //Collection<Route> expected = Arrays.asList(expectedRoute5615ST, expectedRoute5628PZ, expectedRoute5616VD);
        //Collection<Route> result = getRoute.execute(given);

        String json = objectMapper.writeValueAsString(given);
        when(getRoute.execute(given)).thenReturn(Arrays.asList(expectedRoute5615ST,expectedRoute5628PZ,expectedRoute5616VD));

        //THEN
        final MvcResult mvcResult = mockMvc.perform(
                post("/webservicenl/routes")
                        .contentType(APPLICATION_JSON_UTF8)
                        .accept(APPLICATION_JSON_UTF8)
                        .content(json))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();

        verify(getRoute, times(1)).execute(given);

        final Collection<Route> actualRoutes = fromJSON(new TypeReference<Collection<Route>>() {
        }, mvcResult.getResponse().getContentAsString());

        then(actualRoutes.size()).isEqualTo(3);
        then(actualRoutes).containsExactly(expectedRoute5615ST, expectedRoute5628PZ, expectedRoute5616VD);
    }

    private <T> T fromJSON(final TypeReference<T> type, final String jsonPacket) {
        T data;
        try {
            data = new ObjectMapper().readValue(jsonPacket, type);
        } catch (Exception e) {
            throw new RuntimeException();
        }
        return data;
    }
}
