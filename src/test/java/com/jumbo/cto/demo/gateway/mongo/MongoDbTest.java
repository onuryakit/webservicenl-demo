package com.jumbo.cto.demo.gateway.mongo;

import com.jumbo.cto.demo.gateway.mongo.document.StoreDocument;
import com.jumbo.cto.demo.gateway.mongo.repository.StoreRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.index.IndexInfo;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.assertj.core.api.Assertions.fail;
import static org.assertj.core.api.BDDAssertions.then;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = RANDOM_PORT)
public class MongoDbTest {

    private StoreRepository storeRepository;

    @Before
    public void setup(){
        storeRepository = mock(StoreRepository.class);
    }


    @Autowired
    private MongoTemplate mongoTemplate;

    public void shouldCreateIndexesForStoresCollection() {

        // WHEN the application start
        // THEN all the mongo indexes should be created successfully with the proper name
        final Set<String> allowedIndexes = new HashSet<>();
        allowedIndexes.add("_id.postalCode_1__id.city__id.streetName_1");
        allowedIndexes.add("_id.postalCode_1__id.city_1");
        allowedIndexes.add("_id.postalCode_1");
        allowedIndexes.add("createdDate");
        allowedIndexes.add("_id_");

        List<IndexInfo> postalCodeIndexes = mongoTemplate.indexOps("stores").getIndexInfo();
        then(postalCodeIndexes.size()).isEqualTo(7);
        postalCodeIndexes.forEach(indexInfo -> {
            final String indexInfoName = indexInfo.getName();
            if (!allowedIndexes.contains(indexInfoName)) {
                fail("No index with name " + indexInfoName + " allowed");
            }
        });

    }

    @Test
    public void shouldReturnResultFromMongoDbWhenRepositoryContainsMatchingStoredDocument(){
        //GIVEN
        StoreDocument storeDocument = StoreDocument.builder()
                .sapStoreID("1cfa2b5fdf228e20e3a8021x")
                .addressName("Jumbo Eindhoven Centraal")
                .city("Eindhoven")
                .build();

        when(storeRepository.findById(any())).thenReturn(Optional.of(storeDocument));

        //WHEN
        Optional<StoreDocument> actual = storeRepository.findById("1cfa2b5fdf228e20e3a8021x");


        //THEN
        Optional<StoreDocument> expected = Optional.of(StoreDocument.builder()
                .sapStoreID("1cfa2b5fdf228e20e3a8021x")
                .addressName("Jumbo Eindhoven Centraal")
                .city("Eindhoven")
                .build()
        );

        then(actual).isEqualTo(expected);
    }
}
