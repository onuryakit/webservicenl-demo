# Routeplanner API
[![coverage](https://jmb.gitlab.schubergphilis.com/microservices/routeplanner/badges/master/coverage.svg)]()
[![pipeline](https://jmb.gitlab.schubergphilis.com/microservices/routeplanner/badges/master/pipeline.svg)](https://jmb.gitlab.schubergphilis.com/microservices/routeplanner/pipelines)


## About
This is a sample application 


## Types os testing in this application
  This application is developed with 3 different types of testing:
  1. Unit testing: tests the each class with external dependencies mocked
  2. Integration tests: test the real application. This test starts the application, an in memory MongoDB and a Wiremock (to no make real calls to Actual Provider) and consume the endpoints. Afterwards check in mongo if the data has been correctly stored
  3. Stress Testing: Tests the endpoints of the application

## How to
  - Execute UNIT tests of this application and get the JaCoCo coverage of the unit tests
    - Execute the terminal command: `mvn clean test`
    - The result coverage will be inside the `./target/jacoco-ui/index.html` file. Just open this file with any browser
  
  - Execute INTEGRATION tests of this application and get the JaCoCo coverage of the integration tests
    - Execute the command: `mvn clean verify`
    - The result coverage will be inside the `./target/jacoco-it/index.html` file. Just open this file with any browser
    
  - Execute STRESS tests (locally) 
    - Start the application (check how to in details below) 
    - For the rest endpoint, execute the terminal command `mvn clean gatling:test -Dgatling.simulationClass=com.jumbo.simulation.RestSimulation `
   
  - Start the application locally
    - This application require communication with Mongo database. There's a `docker-compose` file to start the MongoDB.
    - Execute the command: `docker-compose up -d`. This will start all the required infrastructure for this application
    - Execute the command `mvn spring-boot:run -Dspring-boot.run.arguments=--spring.profiles.active=dev`
   
### Required parameters do Start the application

| Name                     | Description                     |
|--------------------------|---------------------------------|
| `PARAM_KEY`              | PARAM DESCRIPTION               |
| `SPRING_DATA_MOGODB_URI` | Connection String to MongoDB    |